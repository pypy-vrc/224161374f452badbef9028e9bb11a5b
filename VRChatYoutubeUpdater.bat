@ECHO OFF
SETLOCAL
ECHO ===============================
ECHO VRChat Youtube-dl Updater v0.02
ECHO -------------------------------
ECHO(
ECHO DCinside VRChat Minor Gallery
ECHO by pypy(mina#5656)
ECHO(
FOR /F delims^=^"^ tokens^=2 %%A IN ('REG QUERY "HKCR\VRChat\shell\open\command" /VE') DO SET VRCHAT_PATH=%%A
SET VRCHAT_PATH=%VRCHAT_PATH:~0,-10%
IF NOT EXIST "%VRCHAT_PATH%" SET "VRCHAT_PATH=C:\Program Files (x86)\Steam\steamapps\common\VRChat"
"%VRCHAT_PATH%\VRChat_Data\StreamingAssets\youtube-dl.exe" --update
PING 127.0.0.1 -n 10 -w 1000 > NUL
ECHO(
ECHO Complete!
PAUSE